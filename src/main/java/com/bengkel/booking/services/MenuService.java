package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
    private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
    private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
    private static List<BookingOrder> listAllBookingOrder = new ArrayList<>();

    private static Scanner input = new Scanner(System.in);
    private static Customer currentCustomer;

    public static void run() {
        boolean isLooping = true;
        do {
            String title = "Aplikasi Booking Bengkel";
            PrintService.printTitle(title);
            login();
            if (currentCustomer != null) {
                mainMenu();
            }
        } while (isLooping);

    }

    public static void login() {
        String custId, passwd;
        String title = "Login";
        Scanner in = new Scanner(System.in);
        int attemp = 1;

        do {
            PrintService.printTitle(title);
            System.out.println("Masukkan customer ID:");
            custId = in.nextLine();
            System.out.println("Masukkan password:");
            passwd = in.nextLine();

            currentCustomer = BengkelService.checkCustomerLogin(listAllCustomers, custId, passwd);
            if (currentCustomer != null) break;

            if (attemp == 3) {
                System.out.println("Password salah sebanyak 3 kali.\nMengeluarkan program..");
                System.exit(0);
            }
            attemp++;
        } while (true);
    }

    public static void mainMenu() {
        String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
        int menuChoice = 0;
        boolean isLooping = true;

        do {
            PrintService.printMenu(listMenu, "Booking Bengkel Menu");
            menuChoice = Validation.validasiNumberWithRange("Masukkan Pilihan Menu: ", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length - 1, 0);
            System.out.println(menuChoice);

            switch (menuChoice) {
                case 1:
                    //panggil fitur Informasi Customer
                    BengkelService.customerInfo(currentCustomer);
                    break;
                case 2:
                    //panggil fitur Booking Bengkel
                    BengkelService.bookBengkel(listAllItemService, listAllBookingOrder, listAllCustomers, currentCustomer);
                    break;
                case 3:
                    //panggil fitur Top Up Saldo Coin
                    BengkelService.topupCoin(listAllCustomers, currentCustomer);
                    break;
                case 4:
                    //panggil fitur Informasi Booking Order
                    PrintService.printBooking(listAllBookingOrder);
                    break;
                default:
                    System.out.println("Logout");
                    isLooping = false;
                    break;
            }
        } while (isLooping);


    }

    //Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi

}
