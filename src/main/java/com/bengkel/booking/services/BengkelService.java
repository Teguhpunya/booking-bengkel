package com.bengkel.booking.services;

import com.bengkel.booking.models.*;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.util.*;
import java.util.stream.Collectors;

public class BengkelService {

    //Silahkan tambahkan fitur-fitur utama aplikasi disini

    //Login
    public static Customer checkCustomerLogin(List<Customer> listAllCustomers, String id, String passwd) {
        for (Customer customer : listAllCustomers) {
            boolean idFound = customer.getCustomerId().equalsIgnoreCase(id),
                    passwdMatch = customer.getPassword().equalsIgnoreCase(passwd);
            if (idFound) {
                if (passwdMatch) {
                    return customer;
                } else {
                    System.out.println("Password yang anda masukkan salah!");
                    return null;
                }
            }
        }
        System.out.println("Customer ID tidak ditemukan / salah!");
        return null;
    }

    //Info Customer
    public static void customerInfo(Customer customer) {
        AsciiTable at = new AsciiTable();
        String type = (customer.getClass().getSimpleName().equals("MemberCustomer")) ? "Member" : "Non Member";
        switch (type.toLowerCase()) {
            case "customer":
                System.out.println("\n" + PrintService.formatBold("Non Member"));
                break;
            case "membercustomer":
                System.out.println("\n" + PrintService.formatBold("Member"));
                break;
        }
        PrintService.printTitle(PrintService.formatBold("Customer Profile"));
        at.addRule();
        at.addRow("Customer ID:", customer.getCustomerId());
        at.addRow("Nama:", customer.getName());
        at.addRow("Customer status:", type);
        at.addRow("Alamat:", customer.getAddress());
        if (type.equalsIgnoreCase("Member")) {
            at.addRule();
            String saldoCoin = String.format("%.0f", ((MemberCustomer) customer).getSaldoCoin());
            at.addRow("Saldo coin:", saldoCoin);
        }
        at.addRule();
        System.out.println(at.render());
        PrintService.printVehicle(customer.getVehicles());
    }

    //Booking atau Reservation
    public static void bookBengkel(List<ItemService> serviceList, List<BookingOrder> listAllBookingOrder, List<Customer> listAllCustomers, Customer customer) {
        Scanner in = new Scanner(System.in);
        List<ItemService> selectedServices = new ArrayList<>(List.of());
        List<ItemService> filteredList;

        Vehicle selectedVehicle;
        int maxOrder = customer.getMaxNumberOfService();
        int state = 0;
        double totalPrice = 0;
        double totalPayment = 0;

        PrintService.printTitle("Booking Bengkel");
        while (true) {
            try {
                System.out.println("Masukkan vehicle ID:");
                String id = in.nextLine();
                selectedVehicle = getVehicleById(customer.getVehicles(), id);
                filteredList = getFilteredList(serviceList, selectedVehicle);
                break;
            } catch (Exception e) {
                System.out.println("Vehicle ID tidak ditemukan.");
            }
        }
        servicesInfo(serviceList, selectedVehicle);
        selectService(state, maxOrder, in, filteredList, selectedServices);
        totalPrice = getTotalPrice(selectedServices);
        paymentProcess(listAllBookingOrder, customer, selectedServices, totalPrice, totalPayment);
        listAllCustomers.replaceAll(c -> {
            if (c.getCustomerId().equalsIgnoreCase(customer.getCustomerId())) {
                return customer;
            }
            return c;
        });

    }

    private static void paymentProcess(List<BookingOrder> listAllBookingOrder, Customer customer, List<ItemService> services, double totalPrice, double totalPayment) {
        String regex = "\\b(?=coin)\\w+\\b";
        String payMethod = "Cash";

        if (customer instanceof MemberCustomer) {
            String input = Validation.validasiInput("Silahkan pilih metode pembayaran (saldo coin atau cash): ", "Ketik \"Saldo koin\" atau \"Cash\"", "\\b(?=coin|cash)\\w+\\b");
            payMethod = "Saldo coin";
            double saldoCoin = ((MemberCustomer) customer).getSaldoCoin();
            if (input.matches(regex)) {
                totalPayment = totalPrice - (totalPrice * 0.1);
                ((MemberCustomer) customer).setSaldoCoin(saldoCoin - totalPayment);
            } else {
                totalPayment = totalPrice;
            }
        } else {
            totalPayment = totalPrice;
        }

        addBookingOrderlList(listAllBookingOrder, customer, services, totalPrice, totalPayment, payMethod);

        System.out.println("Booking berhasil!");
        System.out.printf("Total harga service: %.0f\n", totalPrice);
        System.out.printf("Total pembayaran: %.0f\n", totalPayment);
    }

    private static void addBookingOrderlList(List<BookingOrder> listAllBookingOrder, Customer customer, List<ItemService> services, double totalPrice, double totalPayment, String payMethod) {
        BookingOrder bo = createBookingOrder(
                customer,
                services,
                payMethod,
                totalPrice,
                totalPayment
        );
        listAllBookingOrder.add(bo);
    }

    private static BookingOrder createBookingOrder(Customer customer, List<ItemService> serviceList, String paymentMethod, double totalPrice, double totalPayment) {
        Random random = new Random();
        String id = "B-C-" + random.nextInt(255);
        return new BookingOrder(
                id,
                customer,
                serviceList,
                paymentMethod,
                totalPrice,
                totalPayment
        );

    }

    private static double getTotalPrice(List<ItemService> serviceList) {
        double result = 0;
        for (ItemService service : serviceList) {
            result += service.getPrice();
        }
        return result;
    }

    private static void selectService(int state, int maxOrder, Scanner in, List<ItemService> filteredList, List<ItemService> selectedService) {
        while (state < maxOrder) {
            try {
                System.out.printf("Maksimal order: %d kali\n", maxOrder);
                System.out.println("Silahkan masukkan Service ID:");
                String id = in.nextLine();
                String ans = "";
                ItemService temp = getServiceById(filteredList, id);
                if (temp != null) {
                    selectedService.add(temp);
                    state++;
                    if (state < maxOrder) {
                        ans = Validation.validasiInput("Apakah anda ingin menambahkan service lainnya? (Y/N)\n", "Input berupa Y atau N.", "^([YyNn])");
                        if (ans.equalsIgnoreCase("n")) {
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Service ID tidak ditemukan.");
            }
        }
    }

    private static void servicesInfo(List<ItemService> serviceList, Vehicle selectedVehicle) {
        AsciiTable at = new AsciiTable();
        int state = 1;
        List<ItemService> filteredList = getFilteredList(serviceList, selectedVehicle);
        List<String> headers = Arrays.asList("No", "Service ID", "Nama Service", "Tipe Kendaraan", "Harga");

        System.out.printf("\n%s\n", "List service yang tersedia untuk " + selectedVehicle.getVehicleType());
        at.addRule();
        at.addRow(headers).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (ItemService itemService : filteredList) {
            at.addRow(
                    state,
                    itemService.getServiceId(),
                    itemService.getServiceName(),
                    itemService.getVehicleType(),
                    String.format("%.0f", itemService.getPrice())
            );
            at.addRule();
            state++;
        }
        System.out.println(at.render());
    }

    private static List<ItemService> getFilteredList(List<ItemService> serviceList, Vehicle selectedVehicle) {
        return serviceList.stream()
                .filter(itemService -> itemService
                        .getVehicleType()
                        .equalsIgnoreCase(selectedVehicle.getVehicleType())
                )
                .collect(Collectors.toList());
    }

    public static Vehicle getVehicleById(List<Vehicle> vehicleList, String id) {
        return vehicleList.stream()
                .filter(vehicle -> vehicle.getVehiclesId().equalsIgnoreCase(id))
                .findFirst().orElseThrow();
    }

    public static ItemService getServiceById(List<ItemService> serviceList, String id) {
        return serviceList.stream()
                .filter(service -> service.getServiceId().equalsIgnoreCase(id))
                .findFirst().orElseThrow();
    }

    //Top Up Saldo Coin Untuk Member Customer
    public static void topupCoin(List<Customer> listAllCustomers, Customer customer) {
        if (customer instanceof MemberCustomer) {
            double currentSaldo = ((MemberCustomer) customer).getSaldoCoin();
            String title = PrintService.formatBold("Top Up Saldo Coin");
            PrintService.printTitle(title);
            String cashIn = Validation.validasiInput("Masukkan besaran Top Up: ", "Tuliskan hanya berupa angka!", "^[0-9]+");
            ((MemberCustomer) customer).setSaldoCoin(currentSaldo + Double.parseDouble(cashIn));
            listAllCustomers.replaceAll(c -> {
                if (c.getCustomerId().equalsIgnoreCase(customer.getCustomerId())) {
                    return customer;
                }
                return c;
            });
            System.out.printf("Top Up berhasil! Saldo coin anda: %.0f\n", ((MemberCustomer) customer).getSaldoCoin());
        } else {
            System.out.println("Maaf fitur ini hanya berlaku untuk member.");
        }
    }

    //Logout

}
