package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Vehicle;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

public class PrintService {

    public static void printMenu(String[] listMenu, String title) {
        int number = 1;
        String formatTable = " %-2s. %-25s %n";

        printTitle(title);
        printLines(listMenu);
        for (String data : listMenu) {
            if (number < listMenu.length) {
                System.out.printf(formatTable, number, data);
            } else {
                System.out.printf(formatTable, 0, data);
            }
            number++;
        }
        printLines(listMenu);
        System.out.println();
    }

    public static void printBooking(List<BookingOrder> bookingOrderList) {
        AsciiTable at = new AsciiTable();
        int state = 1;
        List<String> headers = new ArrayList<>(Arrays.asList(
                "No",
                "Booking ID",
                "Nama Customer",
                "Payment Method",
                "Total Price",
                "Total Payment",
                "List Service"
        ));

        PrintService.printTitle("Booking Order List");
        at.addRule();
        at.addRow(headers).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (BookingOrder bo : bookingOrderList) {
            StringBuilder services = new StringBuilder();
            for (ItemService s : bo.getServices()) {
                services.append(s.getServiceName());
                if (bo.getServices().size() != 1) services.append(", ");
            }

            List<String> infos = new ArrayList<>(Arrays.asList(
                    String.valueOf(state),
                    bo.getBookingId(),
                    bo.getCustomer().getName(),
                    bo.getPaymentMethod(),
                    String.format("%.0f", bo.getTotalServicePrice()),
                    String.format("%.0f", bo.getTotalPayment()),
                    services.toString()
            ));
            at.addRow(infos);
            at.addRule();
            state++;
        }
        System.out.println(at.render());

    }

    public static void printVehicle(List<Vehicle> vehicleList) {
        AsciiTable at = new AsciiTable();
        List<String> headers = Arrays.asList("No", "Vehicle ID", "Warna", "Tipe Kendaraan", "Tahun");
        int state = 1;

        System.out.println("List kendaraan:");
        at.addRule();
        at.addRow(headers).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (Vehicle vehicle : vehicleList) {
            List<String> infos = new ArrayList<>(Arrays.asList(
                    String.valueOf(state),
                    vehicle.getVehiclesId(),
                    vehicle.getColor(),
                    vehicle.getVehicleType(),
                    String.valueOf(vehicle.getYearRelease())
            ));
            at.addRow(infos);
            at.addRule();
            state++;
        }
        System.out.println(at.render());
    }

    //Silahkan Tambahkan function print sesuai dengan kebutuhan.
    public static void printTitle(String title) {
        int leftLength = 20;
        int half = title.length() / 2;
        String pl = String.valueOf(half + leftLength);
        System.out.printf("\n%" + pl + "s\n", title);
    }

    public static String formatBold(String s) {
        final String setBoldText = "\033[0;1m";
        final String setPlainText = "\033[0;0m";
        return (setBoldText + s + setPlainText);
    }

    public static void printLines(String[] listMenu) {
        int extendedLength = 20,
                maxLength = 0,
                totalLength = 0;
        StringBuilder lines = new StringBuilder();
        for (String s : listMenu) {
            int length = s.length();
            if (maxLength < length) maxLength = length;
        }
        totalLength = maxLength + extendedLength;
        for (int i = 0; i <= totalLength; i++) {
            if (i == 0 || i == totalLength) {
                lines.append("+");
            } else lines.append("-");
        }
        System.out.printf("%s\n", lines);
    }
}
